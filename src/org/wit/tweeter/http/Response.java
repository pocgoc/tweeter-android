package org.wit.tweeter.http;

import java.util.List;

import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

public interface Response<T>
{
  public void setResponse(List<T> aList);
  public void setResponse(T anObject);
  public void errorOccurred (Exception e);
}