package org.wit.tweeter.models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.util.Log;
import static org.wit.android.helpers.LogHelpers.info;

public class Portfolio 
{
	public  List<Tweet>  tweets;
	public  List<User>  users;

	private PortfolioSerializer   serializer;
	public List<Follow> followings;

	public Portfolio(PortfolioSerializer serializer)
	{
		this.serializer = serializer;
		try
		{
			tweets = serializer.loadTweets();
		}
		catch (Exception e)
		{
			info(this, "Error loading residences: " + e.getMessage());
			tweets = new ArrayList<Tweet>();
		}
	}

	public boolean saveTweets()
	{
		try
		{
			serializer.saveTweets(tweets);
			info(this, "Tweets saved to file");
			return true;
		}
		catch (Exception e)
		{
			info(this, "Error saving tweets: " + e.getMessage());
			return false;
		}
	}

	public void addTweet(Tweet tweet)
	{
		tweets.add(tweet);
	}

	public void deleteTweet(Tweet c)
	{
		tweets.remove(c);
	}

	
	public Tweet getTweet(UUID id)
	{
		Log.i(this.getClass().getSimpleName(), "UUID parameter id: "+ id);

		for (Tweet twe : tweets)
		{
			if(id.equals(twe.getId()))
			{
				return twe; 
			}
		}
		return null;
	}   
	 public void updateTweets(List<Tweet>rxd)
	  
	  {
		  tweets.clear();
		  tweets.addAll(rxd);
		  saveTweets(); 
	  }
}
