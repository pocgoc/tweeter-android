package org.wit.tweeter.models;

import java.text.DateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;


public class Tweet 

{
  /**
   * We have changed fields Date and UUID for ease of use in http
   * We could have allowed the service to generate the id as is usual practice
   * However it's more convenient to do so when the Tweet instance is first created
   * Otherwise a blocking http call would be required required in the Tweet constructor below
   * The id is required immediately in the application
   * We send to id the server in the http call and configure the server to allow this practice
   */
  public  String id; //mandates use of getter method getId()
  public  String message;
  public  String datestamp; //mandates use of getter method getDateString()
  public User from;
  public double  zoom       ;//zoom level of accompanying map
public String geolocation;

  /*public Tweet()
  {
    uuid = UUID.randomUUID().toString();
    datestamp = setDate();
    message = "";
  }
  */

  public Tweet ()
  {
	    this.id = UUID.randomUUID().toString();
	    this.datestamp = setDate();
	    this.message = "";
	    this.from = from;
	    this.geolocation 	= "52.253456,-7.187162";
	    this.zoom        = 16.0f;

  }

  /**
   * To minimise code breakage we return id as UUID type
   * Note that the String id is obtained from a UUID type
   * Hence we are allowed to convert back to the original UUID
   * 
   * @return the String id converted to its UUID type
   */
  public UUID getId()
  {
    return UUID.fromString(id);
  }

  /**
   * Retaining this method which was previously used (before refactoring) 
   * 
   * @return
   */
  public String getDateString()
  {
    return datestamp;
  }

  /**
   * 
   * @return formatted date string
   */
  private String setDate()
  {
    DateFormat df = DateFormat.getDateTimeInstance();
    df.setTimeZone(TimeZone.getTimeZone("UTC"));
    String formattedDate = df.format(new Date());
    return formattedDate; 
  }
}

