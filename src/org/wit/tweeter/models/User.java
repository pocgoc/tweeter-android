package org.wit.tweeter.models;
import java.io.Serializable;
import java.util.UUID;

public class User 
{
  public String uuid;
  public String firstName;
  public String lastName;
  public String email;
  public String password;

  public User()
  {}

  public User(String firstName, String lastName, String email, String password)
  {
    this.uuid      = UUID.randomUUID().toString();
    this.firstName = firstName;
    this.lastName  = lastName;
    this.email     = email;
    this.password  = password;
  }

public UUID getId() {
    return UUID.fromString(uuid);
} 

}