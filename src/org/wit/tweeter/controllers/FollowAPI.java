package org.wit.tweeter.controllers;

import static org.wit.android.helpers.LogHelpers.info;

import java.util.List;

import org.wit.tweeter.http.Request;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.http.Rest;
import org.wit.tweeter.httputils.JsonParsers;
import org.wit.tweeter.models.Follow;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

public class FollowAPI
{  

  public static void getFollowings(Context context, Response<Follow> response, String dialogMesssage, User user)
  {
    new GetFollowings(context, response, dialogMesssage).execute(user);
  }

  public static void createFollowing(Context context, Response<Follow> response, String dialogMesssage, User user,  Follow follow)
  {
    new CreateFollowing(context, response, dialogMesssage).execute(user, follow);
  }

  public static void deletefollowing(Context context, Response<Follow> response, String dialogMesssage, User user, Follow follow)
  {
    new DeleteFollowing(context, response, dialogMesssage).execute(user, follow);
  }


}
//===============================================================================================================//
class GetFollowings extends Request
{
  public GetFollowings(Context context, Response<Follow> callback, String message)
  {
    super(context, callback, message);
  }

  @Override
  protected List<Follow> doRequest(Object... params) throws Exception
  {
    String userId = ((User)params[0]).uuid;
    String response =  Rest.get("/api/users/" + userId + "/follow");
    List<Follow> followList = JsonParsers.json2Followings(response);
    return followList;
  }
}

  class CreateFollowing extends Request
  {
    public CreateFollowing(Context context, Response<Follow> callback, String message)
    {
      super(context, callback, message);
    }

    @Override
    protected Follow doRequest(Object... params) throws Exception
    {
      String userId = ((User)params[0]).uuid;
      String response = Rest.post ("/api/users/" + userId + "/follow", JsonParsers.follow2Json(params[1]));
      return JsonParsers.json2Follow(response);
    }
  }

  class DeleteFollowing extends Request 
  {
    public DeleteFollowing(Context context, Response<Follow> callback, String message)
    {
      super(context, callback, message);
    }

    @Override 
    protected Follow doRequest(Object... params) throws Exception

    {
    User user = ((User)params[0]);
      String Followid = ((Follow)params[1]).getId().toString();
      String path    = "/api/users/" + user.uuid + "/follow/" + Followid; 
      String response = Rest.delete (path);
      if(response.equals("success"))
      {
        return new Follow();//String not acceptable argument to any of Response methods so we fake it
      }
      else
      {
        throw new Exception();
      }
    }
  }