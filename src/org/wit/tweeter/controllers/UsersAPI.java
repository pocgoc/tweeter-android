package org.wit.tweeter.controllers;

import static org.wit.android.helpers.LogHelpers.info;

import java.util.List;

import org.wit.tweeter.http.Request;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.http.Rest;
import org.wit.tweeter.httputils.JsonParsers;
import org.wit.tweeter.models.User;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

public class UsersAPI
{  

  public static void getUsers(Context context, Response<User> response, String dialogMesssage)
  {
    new GetUsers(context, response, dialogMesssage).execute();
  }

  public static void createUser(Context context, Response<User> response, String dialogMesssage, User user)
  {
    new CreateUser(context, response, dialogMesssage).execute(user);
  }

  public static void deleteUser(Context context, Response<User> response, String dialogMesssage, User user)
  {
    new DeleteUser(context, response, dialogMesssage).execute(user);
  }


}
//===============================================================================================================//
class GetUsers extends Request
{
  public GetUsers(Context context, Response<User> callback, String message)
  {
    super(context, callback, message);
  }

  @Override
  protected List<User> doRequest(Object... params) throws Exception
  {
    String response =  Rest.get("/api/users");
    List<User> userList = JsonParsers.json2Users(response);
    return userList;
  }
}

class CreateUser extends Request
{
	private static final String TAG = "USERSAPI";

  public CreateUser(Context context, Response<User> callback, String message)
  {
    super(context, callback, message);
  }

  @Override
  protected User doRequest(Object... params) throws Exception
  {
    String response = Rest.post ("/api/users", JsonParsers.user2Json(params[0]));
    Log.v(TAG, "USER=" + response);
    return JsonParsers.json2User(response);
  }
}

class DeleteUser extends Request 
{
  public DeleteUser(Context context, Response<User> callback, String message)
  {
    super(context, callback, message);
  }

  @Override 
  protected User doRequest(Object... params) throws Exception

  {
    String id = ((User)params[0]).uuid.toString();
    String path = "/api/users/" + id; 
    String response = Rest.delete (path);
    if(response.equals("Success Deleted User"))
    {
      return new User();
    }
    else
    {
      throw new Exception();
    }
  }
}