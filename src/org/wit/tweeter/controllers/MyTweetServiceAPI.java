package org.wit.tweeter.controllers;

import java.util.List;

import org.wit.tweeter.http.Request;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.http.Rest;
import org.wit.tweeter.httputils.JsonParsers;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import android.content.Context;
import android.content.Context;
import android.util.Log;

public class MyTweetServiceAPI
{ 
  public static void getTweets(Context context, Response<Tweet> response, String dialogMesssage, User user)
  {
    new GetTweets(context, response, dialogMesssage).execute(user);
  }
  public static void getFollowedTweets(Context context, Response<Tweet> response, String dialogMesssage, User user)
  {
    new GetFollowedTweets(context, response, dialogMesssage).execute(user);
  }
  public static void getAllTweets(Context context, Response<Tweet> response, String dialogMesssage)
  {
    new GetAllTweets(context, response, dialogMesssage).execute();
  }

  public static void createTweet(Context context, Response<Tweet> response, String dialogMesssage, User user, Tweet tweet)
  {
    new CreateTweet(context, response, dialogMesssage).execute(user,tweet);
  }

  public static void editTweet(Context context, Response<Tweet> response, String dialogMesssage, User user, Tweet tweet)
  {
    new EditTweet(context, response, dialogMesssage).execute(user,tweet);
  }
  
  public static void deleteTweet(Context context, Response<Tweet> response, String dialogMesssage, User user, Tweet tweet)
  {
    new DeleteTweet(context, response, dialogMesssage).execute(user, tweet);
  }
}

class GetTweets extends Request
{
  public GetTweets(Context context, Response<Tweet> callback, String message)
  {
    super(context, callback, message);
  }

  @Override
  protected List<Tweet> doRequest(Object... params) throws Exception
  {
    String userId = ((User)params[0]).uuid;
    String response =  Rest.get("/api/users/" + userId + "/tweets");
    List<Tweet> tweetList = JsonParsers.json2Tweets(response);
    return tweetList;
  }
}

class GetFollowedTweets extends Request
{
  public GetFollowedTweets(Context context, Response<Tweet> callback, String message)
  {
    super(context, callback, message);
  }

  @Override
  protected List<Tweet> doRequest(Object... params) throws Exception
  {
    String userId = ((User)params[0]).uuid;
    String response =  Rest.get("/api/users/" + userId + "/followedtweets");
    List<Tweet> tweetList = JsonParsers.json2Tweets(response);
    return tweetList;
  }
}
class GetAllTweets extends Request
{
  public GetAllTweets(Context context, Response<Tweet> callback, String message)
  {
    super(context, callback, message);
  }

  @Override
  protected List<Tweet> doRequest(Object... params) throws Exception
  {
    String response =  Rest.get("/api/tweets");
    List<Tweet> tweets = JsonParsers.json2Tweets(response);
    return tweets;
  }
}
class CreateTweet extends Request
{
  public CreateTweet(Context context, Response<Tweet> callback, String message)
  {
    super(context, callback, message);
  }

  @Override
  protected Tweet doRequest(Object... params) throws Exception
  {
    String userId = ((User)params[0]).uuid;
    String response = Rest.post ("/api/users/" + userId + "/tweets", JsonParsers.tweet2Json(params[1]));
    return JsonParsers.json2Tweet(response);
  }
}

/*class EditTweet extends Request
{
  public EditTweet(Context context, Response<Tweet> callback, String message)
  {
    super(context, callback, message);
  }

  @Override
  protected Tweet doRequest(Object... params) throws Exception
  {
	  User userId = ((User)params[0]);
	    String id = ((Tweet)params[1]).getId().toString();

    String response = Rest.post ("/api/users/" + userId + "/tweet/" + id, JsonParsers.tweet2Json(params[1]));
    return JsonParsers.json2Tweet(response);
  }
}*/

class EditTweet extends Request 
{
	private static final String TAG = "EDIT TWEET";

  public EditTweet(Context context, Response<Tweet> callback, String message)
  {
    super(context, callback, message);
  }

  @Override 
  protected Tweet doRequest(Object... params) throws Exception

  {
  User user = ((User)params[0]);
    String tweetId = ((Tweet)params[1]).getId().toString();
    String path    = "/api/users/" + user.uuid + "/tweet/" + tweetId; 
    String response = Rest.post (path, JsonParsers.tweet2Json(params[1]));
    Log.v(TAG, "response= " + response);

    return JsonParsers.json2Tweet(response);
    
  }
}
class DeleteTweet extends Request 
{
  public DeleteTweet(Context context, Response<Tweet> callback, String message)
  {
    super(context, callback, message);
  }

  @Override 
  protected Tweet doRequest(Object... params) throws Exception

  {
  User user = ((User)params[0]);
    String tweetId = ((Tweet)params[1]).getId().toString();
    String path    = "/api/users/" + user.uuid + "/tweets/" + tweetId; 
    String response = Rest.delete (path);
    if(response.equals("success"))
    {
      return new Tweet();//String not acceptable argument to any of Response methods so we fake it
    }
    else
    {
      throw new Exception();
    }
  }
}




/*public class MyTweetServiceAPI
{ 
	  public static void getTweets(Context context, Response<Tweet> response, String dialogMesssage)
	  {
	    new GetTweets(context, response, dialogMesssage).execute();
	  }

	  public static void createTweet(Context context, Response<Tweet> response, String dialogMesssage, Tweet tweet)
	  {
	    new CreateTweet(context, response, dialogMesssage).execute(tweet);
	  }

	  public static void createUserTweet(Context context, Response<Tweet> response, String dialogMesssage, Tweet tweet)
	  {
	    new CreateUserTweet(context, response, dialogMesssage).execute(tweet);
	  }

	  
	  public static void deleteTweet(Context context, Response<Tweet> response, String dialogMesssage, Tweet tweet)
	  {
	    new DeleteTweet(context, response, dialogMesssage).execute(tweet);
	  }
	}
	================================================================================
class GetTweets extends Request
{
  public GetTweets(Context context, Response<Tweet> callback, String message)
  {
    super(context, callback, message);
  }

  @Override
  protected List<Tweet> doRequest(Object... params) throws Exception
  {
    String response =  Rest.get("/api/tweets");
    List<Tweet> tweets = JsonParsers.json2Tweets(response);
    return tweets;
  }
}

class CreateUserTweet extends Request
{
  public CreateUserTweet(Context context, Response<Tweet> callback, String message)
  {
    super(context, callback, message);
  }

  @Override
  protected Tweet doRequest(Object... params) throws Exception
  {
    String uuid = ((User)params[0]).uuid;
    String response = Rest.post ("/api/users/" + uuid + "/tweets", JsonParsers.tweet2Json(params[1]));
    return JsonParsers.json2Tweet(response);
  }
}


	class CreateTweet extends Request
	{

	  public CreateTweet(Context context,Response<Tweet> callback, String message)
	  {  
	    super(context, callback, message);
	  }

	  @Override
	  protected Tweet doRequest(Object... params) throws Exception
	  {
	    String response = Rest.post ("/api/tweets", JsonParsers.tweet2Json(params[0]));
	    return JsonParsers.json2Tweet(response);
	  }
	}

	class DeleteTweet extends Request 
	{
	  public DeleteTweet(Context context, Response<Tweet> callback, String message)
	  {
	    super(context, callback, message);
	  }

	  @Override 
	  protected Tweet doRequest(Object... params) throws Exception

	  {
	    String id = ((Tweet)params[0]).getId().toString();
	    String path = "/api/tweets/" + id; 
	    String response = Rest.delete (path);
	    if(response.equals("success"))
	    {
	      return new Tweet();
	    }
	    else
	    {
	      throw new Exception();
	    }
	  }
	}*/