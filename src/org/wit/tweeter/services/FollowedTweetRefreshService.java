package org.wit.tweeter.services;

import java.util.List;

import org.wit.android.helpers.LogHelpers;
import org.wit.tweeter.activities.FollwedTweetListFragment;
import org.wit.tweeter.activities.TweetListActivity;
import org.wit.tweeter.activities.TweetListFragment;
import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.http.Rest;
import org.wit.tweeter.httputils.JsonParsers;
import org.wit.tweeter.models.Portfolio;
import org.wit.tweeter.models.Tweet;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;


public class FollowedTweetRefreshService extends IntentService
{
  MyTweeterApp app;
  Portfolio outbox;

  
  public FollowedTweetRefreshService()
  {
    super("RefreshService");
  }

  @Override
  public void onCreate()
  {
    super.onCreate();
    LogHelpers.info(this, "onCreated");
    app = (MyTweeterApp)getApplication();
  }
  
  /*
   * invoked on worker thread(non-Javadoc)
   * @see android.app.IntentService#onHandleIntent(android.content.Intent)
   */
  @Override
  protected void onHandleIntent(Intent intent)
  {
      LogHelpers.info(this, "user : " + app.logged_in_user.uuid);      

    try
    {
      String response =  Rest.get("/api/users/" + app.logged_in_user.uuid + "/followedtweets");//[1]
      List<Tweet> tweetList = JsonParsers.json2Tweets(response);//[2]
//      app.tweets.clear();
      app.tweets  = tweetList; //[3] 
      LogHelpers.info(this, "Tweet list received");//[4]
      broadcastIntent();
    }
    catch(Exception e)//[5]
    {
      LogHelpers.info(this, "failed to retrieve tweets : " + e.getMessage());      
    }    
  }
  private void broadcastIntent()
  {

    Intent localIntent = new Intent(FollwedTweetListFragment.BROADCAST_ACTION);
    // Broadcasts the Intent to receivers in this app.
    LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
  }
  
  @Override
  public void onDestroy()
  {
    super.onDestroy();
    LogHelpers.info(this, "onDestroyed");
  }
}