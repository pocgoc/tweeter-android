package org.wit.tweeter.activities;

import java.util.List;

import org.wit.tweeter.R;
import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.controllers.MyTweetServiceAPI;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.models.Tweet;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity implements Response<Tweet>
{
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    MyTweetServiceAPI.getAllTweets(this, this, "Retreiving Public Tweets");
  }

  public void signinPressed (View view) 
  {
    MyTweeterApp app = (MyTweeterApp) getApplication();
    
    TextView email     = (TextView)  findViewById(R.id.loginEmail);
    TextView password  = (TextView)  findViewById(R.id.loginPassword);
    
    if (app.validUser(email.getText().toString(), password.getText().toString()) != null)
    {
      startActivity (new Intent(this, SelectTimeLine.class));
    }
    else
    {
      Toast toast = Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT);
      toast.show();
    }
  }

@Override
public void setResponse(List<Tweet> aList) {
	// TODO Auto-generated method stub
	
}

@Override
public void setResponse(Tweet anObject) {
	// TODO Auto-generated method stub
	
}

@Override
public void errorOccurred(Exception e) {
	// TODO Auto-generated method stub
	
}
}