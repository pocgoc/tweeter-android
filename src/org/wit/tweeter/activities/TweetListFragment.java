package org.wit.tweeter.activities;


import static org.wit.android.helpers.IntentHelper.navigateUp;

import java.util.ArrayList;
import java.util.List;

import org.wit.android.helpers.IntentHelper;
import org.wit.android.helpers.LogHelpers;
import org.wit.tweeter.activities.TweetFragment;
import org.wit.tweeter.activities.TweetPagerActivity;
import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.models.Portfolio;
import org.wit.tweeter.R;
import org.wit.tweeter.controllers.MyTweetServiceAPI;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import android.widget.ListView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.widget.AdapterView.OnItemClickListener;
import android.view.ActionMode;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.support.v4.content.LocalBroadcastManager;

import org.wit.tweeter.services.RefreshService;


public class TweetListFragment extends ListFragment implements OnItemClickListener, MultiChoiceModeListener, Response<Tweet>
{
	public static final String BROADCAST_ACTION = "org.wit.tweeter.activities.TweetListFragment";
	private List<Tweet> tweets;
	private IntentFilter intentFilter;

	private Portfolio outbox;
	private TweetAdapter adapter;
	private ListView listView;
	private MyTweeterApp  app;
	private User user;

	private static final String TAG = "TWEETLISTFRAGMENT";
	public static   final String  EXTRA_TWEET_ID = "tweeter.TWEET_ID";

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		app = (MyTweeterApp) getActivity().getApplication();
		user   = app.logged_in_user;    
		//refresh
		intentFilter = new IntentFilter(BROADCAST_ACTION);
		registerBroadcastReceiver(intentFilter);

		getActivity().startService(new Intent(getActivity(),RefreshService.class));

	}

	/* ************ MultiChoiceModeListener methods (begin) *********** */
	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu)
	{
		MenuInflater inflater = mode.getMenuInflater();
		inflater.inflate(R.menu.tweet_list_context, menu);
		return true;
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu)
	{
		return false;
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item)
	{
		switch (item.getItemId())
		{
		case R.id.menu_item_delete_tweet:
			removeTweet(mode);
			return true;
		default:
			return false;
		}
	}

	private void removeTweet(ActionMode mode)
	{
		for (int i = adapter.getCount() - 1; i >= 0; i--)
		{
			if (listView.isItemChecked(i))
			{
				Tweet tweet = adapter.getItem(i);
				MyTweetServiceAPI.deleteTweet(getActivity(), this, "deleting tweet", user, tweet); //network call to delete this tweet

			}
		}
		adapter.notifyDataSetChanged();

		adapter = new TweetAdapter(getActivity(), app.tweets);
		setListAdapter(adapter);
		getActivity().startService(new Intent(getActivity(),RefreshService.class));


	}




	@Override
	public void onDestroyActionMode(ActionMode mode)
	{
	}

	@Override
	public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked)
	{
	}

	/* ************ MultiChoiceModeListener methods (end) *********** */



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
	{
		View v = super.onCreateView(inflater, parent, savedInstanceState);
		listView = (ListView) v.findViewById(android.R.id.list);
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		listView.setMultiChoiceModeListener(this);
		return v;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id)
	{
		Tweet twe = ((TweetAdapter) getListAdapter()).getItem(position);
		Intent i = new Intent(getActivity(), TweetPagerActivity.class);
		i.putExtra(TweetFragment.EXTRA_TWEET_ID, twe.getId());
		startActivityForResult(i,0);
	}

	/*
	 * Remove empty tweet from populating tweetList page
	 */
	@Override
	public void onResume()
	{
		{
			try{

				//   	  if(tweets != null && tweets.size() > 0)
				//        tweets.clear();

				super.onResume();
				getActivity().startService(new Intent(getActivity(),RefreshService.class));
				adapter.notifyDataSetChanged();

			}
			catch (Exception e)
			{
				super.onResume();
				setHasOptionsMenu(true);
				getActivity().setTitle(R.string.app_name);

				MyTweeterApp app = (MyTweeterApp) getActivity().getApplication();
				outbox = app.portfolio;
				app.tweets = outbox.tweets;

				adapter = new TweetAdapter(getActivity(), app.tweets);
				setListAdapter(adapter);
			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.tweetlist, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case android.R.id.home:
		      navigateUp(getActivity());
		      return true;
		case R.id.menu_item_new_tweet:
			Tweet tweet = new Tweet();
			outbox.addTweet(tweet);

			Intent i = new Intent(getActivity(), NewTweetPagerActivity.class);
			i.putExtra(NewTweetFragment.EXTRA_TWEET_ID, tweet.getId());
			startActivityForResult(i, 0);
			adapter.notifyDataSetChanged();
			return true;

		case R.id.action_settings:
			Intent j = new Intent(getActivity(), SettingsActivity.class);
			startActivityForResult(j, 0);
			return true;

		case R.id.action_refresh:    
			getActivity().startService(new Intent(getActivity(),RefreshService.class));
			return true;

		case R.id.action_clearList : 
			adapter.clear();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		Tweet tweet = adapter.getItem(position);
		IntentHelper.startActivityWithData(getActivity(), TweetPagerEditActivity.class, "TWEET_ID", tweet.getId());
	}


	@Override
	public void setResponse(List<Tweet> aList)
	{

		String msg = "tweet retrival success";
		Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();  
		LogHelpers.info(this, msg);

		app.tweets = aList;
		Log.v(TAG, "Applist list size 1=" + app.tweets.size());
		adapter.clear();

		adapter = new TweetAdapter(getActivity(), app.tweets);
		//Storing our list of tweets to the Portfolio serializer internal memory
		outbox.tweets = aList;
		//app.tweets = outbox.tweets;
		setListAdapter(adapter);
		adapter.tweets = aList;
		Log.v(TAG, "Addapter tweet list size =" + adapter.tweets.size());
		adapter.notifyDataSetChanged();


	}

	@Override
	public void setResponse(Tweet anObject)
	{
		String msg = "tweet deletion success";
		Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();  
		LogHelpers.info(this, msg);
	}

	@Override
	public void errorOccurred(Exception e)
	{
		String msg = "Failure";
		Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();  
		LogHelpers.info(this, msg + " : " + e.getMessage());
	}
	private void registerBroadcastReceiver(IntentFilter intentFilter)
	{
		ResponseReceiver mResponseReceiver = new ResponseReceiver();
		// Registers the ResponseReceiver and its intent filters
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mResponseReceiver, intentFilter);

	}
	//Broadcast receiver for receiving status updates from the IntentService
	private class ResponseReceiver extends BroadcastReceiver
	{
		private void ResponseReceiver() {}
		// Called when the BroadcastReceiver gets an Intent it's registered to receive
		@Override
		public void onReceive(Context context, Intent intent)
		{
			try{

				   	  if(tweets != null && tweets.size() > 0)
				        tweets.clear();
				adapter = new TweetAdapter(getActivity(), app.tweets);
				setListAdapter(adapter);
				outbox.tweets = app.tweets;



//				adapter.clear();
				Log.v(TAG, "Addapter tweet list size =" + adapter.tweets.size());

				Log.v(TAG, "Addapter tweet list size =" + app.tweets.size());

//				tweets.clear();
			//adapter.tweets.clear();
				/*adapter.tweets = app.tweets;
				Log.v(TAG, "Applist list size =" + app.tweets.size());			//Storing our list of tweets to the Portfolio serializer internal memory
				outbox.tweets = app.tweets;
				setListAdapter(adapter);
				Log.v(TAG, "Addapter tweet list size =" + adapter.tweets.size());
				adapter.notifyDataSetChanged();
				
*/
				adapter.tweets = app.tweets;
			      adapter.notifyDataSetChanged();
			}
			catch (Exception e)
			{
				setHasOptionsMenu(true);
			
//				getActivity().setTitle(R.string.app_name);

//				MyTweeterApp app = (MyTweeterApp) getActivity().getApplication();
//				outbox = app.portfolio;
//				app.tweets = outbox.tweets;

//				adapter = new TweetAdapter(getActivity(), app.tweets);
				setListAdapter(adapter);
			}
		}
	}

}
class TweetAdapter extends ArrayAdapter<Tweet>
{
	private Context        context;
	public  List<Tweet> tweets;

	public TweetAdapter(Context context, List<Tweet> tweets)
	{
		super(context, R.layout.activity_tweetlist, tweets);
		this.context   = context;
		this.tweets = tweets;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
		{
			convertView = inflater.inflate(R.layout.list_item_tweet, null);
		}
		Tweet twe = getItem(position);

		TextView tweetText = (TextView) convertView.findViewById(R.id.tweet_list_item_TweetMesage);
		tweetText.setText(twe.message);

		TextView dateTextView = (TextView) convertView.findViewById(R.id.tweet_list_item_dateTextView);
		dateTextView.setText(twe.getDateString());

		return convertView;
	}

	@Override
	public int getCount()
	{
		return tweets.size();
	}
}