package org.wit.tweeter.activities;


import static org.wit.android.helpers.IntentHelper.navigateUp;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.wit.android.helpers.IntentHelper;
import org.wit.android.helpers.LogHelpers;
import org.wit.tweeter.activities.TweetFragment;
import org.wit.tweeter.activities.TweetPagerActivity;
import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.models.Portfolio;
import org.wit.tweeter.R;
import org.wit.tweeter.controllers.FollowAPI;
import org.wit.tweeter.controllers.MyTweetServiceAPI;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.models.Follow;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import android.widget.ListView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.widget.AdapterView.OnItemClickListener;
import android.view.ActionMode;
import android.widget.AbsListView.MultiChoiceModeListener;


public class FollwedUsersFragment extends ListFragment implements OnItemClickListener, MultiChoiceModeListener, Response<Follow>
{
	public static Intent BROADCAST_ACTION;
	private List<Follow> followings;


	private Portfolio outbox;
	private FollowAdapter adapter;
	private ListView listView;
	private MyTweeterApp  app;
	private User user;
	private User currentlyloggedin;
	private Follow follow;

	private static final String TAG = "FollowedUserTFRAGMENT";
	public static   final String  EXTRA_TWEET_ID = "tweeter.TWEET_ID";
	public static final String EXTRA_FOLLOW_ID = "tweeter.FOLLOW_ID";
	public static final String EXTRA_USER_ID = "tweeter.USER_ID";

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		app = (MyTweeterApp) getActivity().getApplication();    
		currentlyloggedin = app.logged_in_user;
		

		FollowAPI.getFollowings(getActivity(), this, "Retreiving my Followings", app.logged_in_user);
	}

	/* ************ MultiChoiceModeListener methods (begin) *********** */
	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu)
	{
		MenuInflater inflater = mode.getMenuInflater();
		inflater.inflate(R.menu.tweet_list_context, menu);
		return true;
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu)
	{
		return false;
	}

	  @Override
	  public boolean onActionItemClicked(ActionMode mode, MenuItem item)
	  {
	    switch (item.getItemId())
	    {
	    case R.id.menu_item_delete_tweet:
	      removeTweet(mode);
	      return true;
	    default:
	      return false;
	    }
	    }
	  
	    private void removeTweet(ActionMode mode)
	    {
	      for (int i = adapter.getCount() - 1; i >= 0; i--)
	      {
	        if (listView.isItemChecked(i))
	        {
	          Follow follow = adapter.getItem(i);
	          FollowAPI.deletefollowing(getActivity(),this, "Unfollowing", currentlyloggedin, follow);
	          FollowAPI.getFollowings(getActivity(), this, "refrshing list", currentlyloggedin);
	        }
	      }
		  adapter.notifyDataSetChanged();

	      adapter = new FollowAdapter(getActivity(), app.followings);
	      setListAdapter(adapter);

	    }




		@Override
		public void onDestroyActionMode(ActionMode mode)
		{
		}

		@Override
		public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked)
		{
		}

		/* ************ MultiChoiceModeListener methods (end) *********** */



		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
		{
			View v = super.onCreateView(inflater, parent, savedInstanceState);
			listView = (ListView) v.findViewById(android.R.id.list);
			listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
			listView.setMultiChoiceModeListener(this);
			return v;
		}

		@Override
		public void onListItemClick(ListView l, View v, int position, long id)
		{

		}
		@Override
		public void onResume()
		{
			{
				try{

					//   	  if(tweets != null && tweets.size() > 0)
					//        tweets.clear();

					super.onResume();
					adapter.notifyDataSetChanged();

				}
				catch (Exception e)
				{
					super.onResume();
					setHasOptionsMenu(true);
					getActivity().setTitle(R.string.app_name);

					MyTweeterApp app = (MyTweeterApp) getActivity().getApplication();
					outbox = app.portfolio;
					//app.followings = outbox.followings;

					adapter = new FollowAdapter(getActivity(), app.followings);
					setListAdapter(adapter);
				}
			}
		}

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
		{
			super.onCreateOptionsMenu(menu, inflater);
			inflater.inflate(R.menu.tweetlist, menu);
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item)
		{
			switch (item.getItemId())
			{
			case R.id.menu_item_new_tweet:
				Tweet tweet = new Tweet();
				outbox.addTweet(tweet);

				Intent i = new Intent(getActivity(), NewTweetPagerActivity.class);
				i.putExtra(NewTweetFragment.EXTRA_TWEET_ID, tweet.getId());
				startActivityForResult(i, 0);
				return true;

			case R.id.action_settings:
				Intent j = new Intent(getActivity(), SettingsActivity.class);
				startActivityForResult(j, 0);
				return true;

			case R.id.action_clearList : 
				adapter.clear();
				return true;
			case android.R.id.home:
			      navigateUp(getActivity());
			      return true;
			default:
				return super.onOptionsItemSelected(item);
			}
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			Follow follow = adapter.getItem(position);
			IntentHelper.startActivityWithData(getActivity(), TweetPagerEditActivity.class, "FOLLOW_ID", follow.getId());
		}



		@Override
		public void setResponse(List<Follow> aList)
		{
			app.followings = aList;

			String msg = "Following retrival success";
			Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();  
			LogHelpers.info(this, msg);

			//outbox.followings.clear();
			adapter.clear();
			Log.v(TAG, "Applist list size =" + app.tweets.size());
			adapter = new FollowAdapter(getActivity(), app.followings);
			//Storing our list of tweets to the Portfolio serializer internal memory
			outbox.followings = aList;
			//app.tweets = outbox.tweets;
			setListAdapter(adapter);
			adapter.followings = aList;
			Log.v(TAG, "Addapter tweet list size =" + adapter.followings.size());
			adapter.notifyDataSetChanged();



		}

		@Override
		public void setResponse(Follow anObject)
		{
			String msg = "tweet deletion success";
			Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();  
			LogHelpers.info(this, msg);
		}

		@Override
		public void errorOccurred(Exception e)
		{
			String msg = "Failure";
			Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();  
			LogHelpers.info(this, msg + " : " + e.getMessage());
		}

	}

class FollowAdapter extends ArrayAdapter<Follow>
{
	private Context        context;
	public  List<Follow> followings;

	public FollowAdapter(Context context, List<Follow> followings)
	{
		super(context, R.layout.activity_tweetlist, followings);
		this.context   = context;
		this.followings = followings;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
		{
			convertView = inflater.inflate(R.layout.list_item_tweet, null);
		}
		Follow twe = getItem(position);

		TextView tweetText = (TextView) convertView.findViewById(R.id.tweet_list_item_TweetMesage);
		tweetText.setText(twe.targetUser.firstName + " " + twe.targetUser.lastName);

		TextView dateTextView = (TextView) convertView.findViewById(R.id.tweet_list_item_dateTextView);
		dateTextView.setText(twe.Followid);

		return convertView;
	}

	@Override
	public int getCount()
	{
		return followings.size();
	}
}