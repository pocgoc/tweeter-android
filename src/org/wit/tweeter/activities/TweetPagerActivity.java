package org.wit.tweeter.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.wit.tweeter.R;
import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.models.Portfolio;
import org.wit.tweeter.models.Tweet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import static org.wit.android.helpers.LogHelpers.info;



public class TweetPagerActivity extends FragmentActivity  implements ViewPager.OnPageChangeListener
{
  private ViewPager viewPager;
  private List<Tweet> tweets;  
  private Portfolio portfolio;
  private PagerAdapter pagerAdapter;



  @Override
  public void onCreate(Bundle savedInstanceState) 
  {
	  super.onCreate(savedInstanceState);
	    viewPager = new ViewPager(this);
	    viewPager.setId(R.id.viewPager);
	    setContentView(viewPager);
	    setTweetList();
	    pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tweets);
	    viewPager.setAdapter(pagerAdapter);
	    viewPager.setOnPageChangeListener(this); 
	    setCurrentItem();
  }
  
  /*
   * Ensure selected tweet is shown in details view
   */
  private void setCurrentItem()
  {
    //On pressing back button display move to position on list that includes current tweet
    UUID res = (UUID) getIntent().getSerializableExtra(TweetFragment.EXTRA_TWEET_ID);
    for (int i = 0; i < tweets.size(); i++)
    {
      if (tweets.get(i).getId().toString().equals(res.toString()))
      {
        viewPager.setCurrentItem(i);
        break;
      }
    }
  }
  
  private void setTweetList()
  {
    MyTweeterApp app = (MyTweeterApp) getApplication();
    portfolio = app.portfolio; 
    tweets = portfolio.tweets;    
  }

@Override
public void onPageScrollStateChanged(int arg0) {
	// TODO Auto-generated method stub
	
}

@Override
public void onPageScrolled(int arg0, float arg1, int arg2) 
{
	info(this, "onPageScrolled: arg0 "+arg0+" arg1 "+arg1+" arg2 "+arg2);
    Tweet tweet = tweets.get(arg0);
    if (tweet.message != null)
    {
      setTitle(tweet.message);
    }	
}

@Override
public void onPageSelected(int arg0) {
	// TODO Auto-generated method stub
	
}
  
class PagerAdapter extends FragmentStatePagerAdapter 
{
  private List<Tweet>  tweets; 

  public PagerAdapter(FragmentManager fm, List<Tweet> tweets)
  {
    super(fm);
    this.tweets = tweets;
  }

  @Override
  public int getCount()  
  {  
    return tweets.size();  
  }

  @Override
  public Fragment getItem(int pos) 
  {
    Tweet tweet = tweets.get(pos);
    Bundle args = new Bundle();
    args.putSerializable(TweetFragment.EXTRA_TWEET_ID, tweet.getId());
    TweetFragment fragment = new TweetFragment();
    fragment.setArguments(args);
    return fragment;
  } 
}
}
