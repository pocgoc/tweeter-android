package org.wit.tweeter.activities;
import org.wit.tweeter.R;

import static org.wit.android.helpers.IntentHelper.navigateUp;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener
{
  private SharedPreferences settings;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    addPreferencesFromResource(R.xml.settings);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
    return super.onCreateView(inflater, container, savedInstanceState);
  }
  @Override
  public void onStart()
  {
    super.onStart();
    settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
    settings.registerOnSharedPreferenceChangeListener(this);
  }
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
    case android.R.id.home:
      navigateUp(getActivity());
      return true;
    default:
      return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
  {
  }
}