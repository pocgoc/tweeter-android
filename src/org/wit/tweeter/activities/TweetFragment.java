package org.wit.tweeter.activities;

import java.util.List;
import java.util.UUID;













import org.wit.android.helpers.LogHelpers;
import org.wit.android.helpers.MapHelper;
import org.wit.tweeter.R;
import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.controllers.MyTweetServiceAPI;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.models.Portfolio;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import static org.wit.android.helpers.ContactHelper.getDisplayName;
import static org.wit.android.helpers.ContactHelper.getEmail;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;
import static org.wit.android.helpers.IntentHelper.sendEmail;
import static org.wit.android.helpers.IntentHelper.navigateUp;



public class TweetFragment extends Fragment implements GoogleMap.OnMarkerDragListener, GoogleMap.OnCameraChangeListener

{
	private static final String TAG = "TWEET PAGE";

  public static   final String  EXTRA_TWEET_ID = "tweeter.TWEET_ID";
  public static   final String  EXTRA_USER_ID = "tweeter.USER_ID";

  private static final int REQUEST_CONTACT = 1;
  
  private TextView tweetText;
  private TextView textCount;
  
  private Tweet tweet;
  private Portfolio outbox;
  private TextView dateTime;
  private TextView TweetFrom;


  private MyTweeterApp  app;

private User user;

//Map
SupportMapFragment mapFragment;
GoogleMap gmap;
Marker marker;
LatLng markerPosition;
boolean markerDragged;

//GPS
private LocationManager locationManager;
private String provider;


  
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    UUID userId = (UUID)getArguments().getSerializable(EXTRA_TWEET_ID);
    //UUID tweId = (UUID)getActivity().getIntent().getSerializableExtra(EXTRA_USER_ID);
	//Log.v("LOGS", "This is tweet " + tweet.id );
    app = (MyTweeterApp) getActivity().getApplication();
    outbox = app.portfolio; 
    if (userId != null)
    {
    tweet = app.getTweet(userId);
    }
	Log.v("LOGS", "This is tweet " + tweet.id );

	Log.v("LOGS", "This is user of this tweet " + tweet.from);

    user = app.getUser(tweet.from.getId());  
    // Get the Intialize location manager
   
    

  }
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
  {
    View v = inflater.inflate(R.layout.fragment_view_tweet, parent, false);

    getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
    addListeners(v);
    updateControls(tweet);

    return v;
  } 
  
  private void addListeners(View v)
  {
	TweetFrom    = (TextView) v.findViewById(R.id.TweetFrom);
    tweetText    = (TextView) v.findViewById(R.id.TweetMessage);
    textCount    = (TextView) v.findViewById(R.id.TweetCharacters);
    dateTime  = (TextView)   v.findViewById(R.id.DateAndTime);
   }
 
  public void updateControls(Tweet tweet)
  {
	    TweetFrom.setText("TWEET FROM :" + "\nFirstname: " + user.firstName + "\nLastname :" + user.lastName);
    tweetText.setText("TWEET MESSAGE :\n" + tweet.message);
    dateTime.setText("DATE SENT :\n" + tweet.getDateString());
    int count = 140 - tweetText.length();
    textCount.setText("CHARACTERS USED :\n" + Integer.toString(count));
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
    case android.R.id.home: navigateUp(getActivity());
                            return true;
    default:                return super.onOptionsItemSelected(item);
    }
  }
  
  @Override
  public void onPause()
  {
    super.onPause();
    outbox.saveTweets();
    //locationManager.removeUpdates(this);

  }
  
  
  /* Request updates at startup */
  @Override
  public void onResume() 
  {
    super.onResume();
    //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, this);
  }
  //--------------------------------MAPS GPS-------------------------------------

  //------------------- iNtialize Map Fragment---------------------
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) 
  {
    super.onActivityCreated(savedInstanceState);
    initializeMapFragment();
  }
  private void initializeMapFragment()
  {
    FragmentManager fm = getChildFragmentManager();
    mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
    if (mapFragment == null)
    {
      mapFragment = SupportMapFragment.newInstance();
      fm.beginTransaction().replace(R.id.map, mapFragment).commit();
    }
  }
  
  @Override
  public void onMarkerDrag(Marker arg0)
  {
  }

  @Override
  public void onMarkerDragEnd(Marker arg0)
  {
    tweet.geolocation = MapHelper.latLng(arg0.getPosition());
    getActivity().setTitle(tweet.geolocation);
    gmap.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
    markerDragged = true;
  }

  @Override
  public void onMarkerDragStart(Marker arg0)
  {
  }

  @Override
  public void onCameraChange(CameraPosition arg0)
  {
    tweet.zoom = arg0.zoom;
    markerPosition = MapHelper.latLng(getActivity(), tweet.geolocation);
    if (marker != null)
    {
      marker.remove();
      marker = null;
    }

    marker = gmap.addMarker(new MarkerOptions().position(markerPosition).draggable(true).title("Residence").alpha(0.7f)
        .snippet("GPS : " + markerPosition.toString()));

  }
  private void renderMap(LatLng markerPosition)
  {
    if (mapFragment != null)
    {
      gmap = mapFragment.getMap();
      if (gmap != null)
      {
        gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(markerPosition, (float) tweet.zoom));
        gmap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        gmap.setOnMarkerDragListener(this);
        gmap.setOnCameraChangeListener(this);
      }
    }
  }
  
  @Override
  public void onStart()
  {
    super.onStart();
    Log.v(TAG, "Tweet GeoLocation = " + tweet.from.firstName);
    Log.v(TAG, "Tweet GeoLocation = " + tweet.geolocation);

    renderMap(MapHelper.latLng(getActivity(), tweet.geolocation));
    gmap.setOnCameraChangeListener(this);
  }
  
  
  
}
