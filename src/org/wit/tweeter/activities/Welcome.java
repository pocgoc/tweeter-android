package org.wit.tweeter.activities;

import java.util.List;

import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.R;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.controllers.MyTweetServiceAPI;
import org.wit.tweeter.controllers.UsersAPI;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Welcome extends Activity implements Response<User>
{
  MyTweeterApp app;
  
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_welcome);
    app = (MyTweeterApp) getApplication();
    
    UsersAPI.getUsers(this, this, "Retrieving list of users");
  }
  
  @Override
  public void onResume()
  {
    super.onResume();
    app.logged_in_user = null;
    UsersAPI.getUsers(this, this, "Retrieving list of users");
  }
  
  void serviceUnavailableMessage()
  {
    Toast toast = Toast.makeText(this, "Your Tweeter Service Unavailable. Try again later", Toast.LENGTH_LONG);
    toast.show();
  }
  
  public void loginPressed (View view) 
  {
    if (app.TweeterServiceAvailable)
    {
      startActivity (new Intent(this, Login.class));
    }
    else
    {
      serviceUnavailableMessage();
    }
  }

  public void signupPressed (View view) 
  {
    if (app.TweeterServiceAvailable)
    {
      startActivity (new Intent(this, Signup.class));
    }
    else
    {
      serviceUnavailableMessage();
    }
  }
  
  @Override
  public void setResponse(List<User> aList)
  {
    app.users = aList;
    app.TweeterServiceAvailable = true;
  }
  
  @Override
  public void errorOccurred(Exception e)
  {
    app.TweeterServiceAvailable = false;
    serviceUnavailableMessage();
  }
  
  @Override
  public void setResponse(User anObject)
  {}



}

