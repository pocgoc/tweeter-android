



package org.wit.tweeter.activities;


import static org.wit.android.helpers.IntentHelper.navigateUp;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.wit.android.helpers.IntentHelper;
import org.wit.android.helpers.LogHelpers;
import org.wit.tweeter.activities.TweetFragment;
import org.wit.tweeter.activities.TweetPagerActivity;
import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.models.Portfolio;
import org.wit.tweeter.R;
import org.wit.tweeter.controllers.MyTweetServiceAPI;
import org.wit.tweeter.controllers.UsersAPI;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import android.widget.ListView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.widget.AdapterView.OnItemClickListener;
import android.view.ActionMode;
import android.widget.AbsListView.MultiChoiceModeListener;


public class UserListFragment extends ListFragment implements OnItemClickListener, MultiChoiceModeListener, Response<User>
{
  public static Intent BROADCAST_ACTION;
  private List<Tweet> tweets;

  private Portfolio outbox;
  private UserAdapter adapter;
  private ListView listView;
  private MyTweeterApp  app;
  private User user;

	private static final String TAG = "USERLISTFRAGMENT";
	  public static   final String  EXTRA_USERER_ID = "tweeter.USERER_ID";


  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    app = (MyTweeterApp) getActivity().getApplication();
         user   = app.logged_in_user;    
       UsersAPI.getUsers(getActivity(), this, "Retrieving list of users");
    
  }

  /* ************ MultiChoiceModeListener methods (begin) *********** */
  @Override
  public boolean onCreateActionMode(ActionMode mode, Menu menu)
  {
    MenuInflater inflater = mode.getMenuInflater();
    inflater.inflate(R.menu.tweet_list_context, menu);
    return true;
  }

  @Override
  public boolean onPrepareActionMode(ActionMode mode, Menu menu)
  {
    return false;
  }

  @Override
  public boolean onActionItemClicked(ActionMode mode, MenuItem item)
  {
    switch (item.getItemId())
    {
    case R.id.menu_item_delete_tweet:
      removeUser(mode);
      return true;
    default:
      return false;
    }
    }
  
    private void removeUser(ActionMode mode)
    {
      for (int i = adapter.getCount() - 1; i >= 0; i--)
      {
        if (listView.isItemChecked(i))
        {
          User user = adapter.getItem(i);
          //MyTweetServiceAPI.deleteTweet(getActivity(), this, "deleting user", user, tweet); //network call to delete this tweet
        }
      }
	  adapter.notifyDataSetChanged();

      adapter = new UserAdapter(getActivity(), app.users);
      setListAdapter(adapter);

    }
    
  


  @Override
  public void onDestroyActionMode(ActionMode mode)
  {
  }

  @Override
  public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked)
  {
  }

  /* ************ MultiChoiceModeListener methods (end) *********** */
  
  
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
  {
    View v = super.onCreateView(inflater, parent, savedInstanceState);
    listView = (ListView) v.findViewById(android.R.id.list);
    listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    listView.setMultiChoiceModeListener(this);
    return v;
  }

  @Override
  public void onListItemClick(ListView l, View v, int position, long id)
  {
	  User user = ((UserAdapter) getListAdapter()).getItem(position);
	    Intent i = new Intent(getActivity(), UserTweetListActivity.class);
	    i.putExtra(UserTweetListFragment.EXTRA_USER_ID, user.getId());
		Log.v("log", "This is a problem" + UserTweetListFragment.EXTRA_USER_ID);
		Log.v("log", "This is a problem user.getId" + user.getId());
	startActivityForResult(i,0);
  }
  
 /*
  * Remove empty tweet from populating tweetList page
  */
  @Override
  public void onResume()
  {
	  try{
			
			super.onResume();
			adapter.notifyDataSetChanged();

		}
		catch (Exception e)
		{
			super.onResume();
			adapter = new UserAdapter(getActivity(), app.users);
			setListAdapter(adapter);
		}
	}
  

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
  {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.tweetlist, menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
    case R.id.menu_item_new_tweet:
      Tweet tweet = new Tweet();
      outbox.addTweet(tweet);

		Intent i = new Intent(getActivity(), NewTweetPagerActivity.class);
		i.putExtra(NewTweetFragment.EXTRA_TWEET_ID, tweet.getId());
		startActivityForResult(i, 0);
      return true;
      
    case R.id.action_settings:
      Intent j = new Intent(getActivity(), SettingsActivity.class);
      startActivityForResult(j, 0);
      return true;
  
    case R.id.action_clearList : 
      adapter.clear();
      return true;
    case android.R.id.home:
        navigateUp(getActivity());
        return true;
      
    default:
      return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id)
  {
    User user = adapter.getItem(position);
    IntentHelper.startActivityWithData(getActivity(), TweetPagerEditActivity.class, "USER_ID", user.getId());
  }


  class UserAdapter extends ArrayAdapter<User>
  {
    private Context        context;
    public  List<User> users;

    public UserAdapter(Context context, List<User> users)
    {
      super(context, R.layout.activity_usertlist, users);
      this.context   = context;
      this.users = users;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
      LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      if (convertView == null)
      {
        convertView = inflater.inflate(R.layout.list_item_user, null);
      }
      User use = getItem(position);

      TextView userText = (TextView) convertView.findViewById(R.id.User_list_item_nameview);
      userText.setText("User Firstname & LastName: " + use.firstName + " " + use.lastName);

      TextView emailTextView = (TextView) convertView.findViewById(R.id.user_list_item_emailview);
      emailTextView.setText("User email: " + use.email);

      return convertView;
    }

/*    @Override
    public int getCount()
    {
      return users.size();
    }*/
  }



  @Override
  public void errorOccurred(Exception e)
  {
    String msg = "Failure";
    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();  
    LogHelpers.info(this, msg + " : " + e.getMessage());
  }


@Override
public void setResponse(User anObject) {
	// TODO Auto-generated method stub
	
}

@Override
public void setResponse(List<User> aList) {
	String msg = "tweet retrival success";
	  Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();  
	  LogHelpers.info(this, msg);

	    app.users = aList;
	    Log.v(TAG, "Applist list size =" + app.tweets.size());
	    adapter.clear();

	    adapter = new UserAdapter(getActivity(), app.users);
	   //Storing our list of tweets to the Portfolio serializer internal memory
//	    outbox.users = aList;
	     //app.tweets = outbox.tweets;
	    setListAdapter(adapter);
	    adapter.users = aList;
		Log.v(TAG, "Addapter tweet list size =" + adapter.users.size());
	    adapter.notifyDataSetChanged();	
	// TODO Auto-generated method stub
	
}


}