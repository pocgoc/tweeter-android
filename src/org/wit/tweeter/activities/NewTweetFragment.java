package org.wit.tweeter.activities;

import java.util.List;
import java.util.UUID;










//Map Imports
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.SupportMapFragment;
















import org.wit.android.helpers.LogHelpers;
import org.wit.android.helpers.MapHelper;
import org.wit.tweeter.R;
import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.controllers.MyTweetServiceAPI;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.models.Portfolio;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import static org.wit.android.helpers.ContactHelper.getDisplayName;
import static org.wit.android.helpers.ContactHelper.getEmail;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;
import static org.wit.android.helpers.IntentHelper.sendEmail;
import static org.wit.android.helpers.IntentHelper.navigateUp;



public class NewTweetFragment extends Fragment implements TextWatcher, OnClickListener, Response<Tweet>, GoogleMap.OnMarkerDragListener, GoogleMap.OnCameraChangeListener, LocationListener

{
	private static final String TAG = "TWEET PAGE";

  public static   final String  EXTRA_TWEET_ID = "tweeter.TWEET_ID";
  public static   final String  EXTRA_USER_ID = "tweeter.USER_ID";
  private static final int REQUEST_CONTACT = 1;
  
  private EditText geolocation;

  private EditText tweetText;
  private Button tweetButton;
  private TextView textCount;
  
  private Tweet tweet;
  private Portfolio outbox;
  private TextView dateTime;
  private MyTweeterApp  app;
  private User user;
  
  // Map
  SupportMapFragment mapFragment;
  GoogleMap gmap;
  Marker marker;
  LatLng markerPosition;
  boolean markerDragged;
  
  //GPS
  private LocationManager locationManager;
  private String provider;




  
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    UUID userId = (UUID)getArguments().getSerializable(EXTRA_TWEET_ID);
    app = (MyTweeterApp) getActivity().getApplication();
    user   = app.logged_in_user;    
    outbox = app.portfolio; 
    tweet = outbox.getTweet(userId); 



  }
  //------------------- iNtialize Map Fragment---------------------
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) 
  {
    super.onActivityCreated(savedInstanceState);
    initializeMapFragment();
  }
  private void initializeMapFragment()
  {
    FragmentManager fm = getChildFragmentManager();
    mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
    if (mapFragment == null)
    {
      mapFragment = SupportMapFragment.newInstance();
      fm.beginTransaction().replace(R.id.map, mapFragment).commit();
    }
  }
  
  @Override
  public void onMarkerDrag(Marker arg0)
  {
  }

  @Override
  public void onMarkerDragEnd(Marker arg0)
  {
    tweet.geolocation = MapHelper.latLng(arg0.getPosition());
    getActivity().setTitle(tweet.geolocation);
    gmap.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
    markerDragged = true;
  }

  @Override
  public void onMarkerDragStart(Marker arg0)
  {
  }

  @Override
  public void onCameraChange(CameraPosition arg0)
  {
    tweet.zoom = arg0.zoom;
    markerPosition = MapHelper.latLng(getActivity(), tweet.geolocation);
    if (marker != null)
    {
      marker.remove();
      marker = null;
    }

    marker = gmap.addMarker(new MarkerOptions().position(markerPosition).draggable(true).title("Residence").alpha(0.7f)
        .snippet("GPS : " + markerPosition.toString()));

  }
  private void renderMap(LatLng markerPosition)
  {
    if (mapFragment != null)
    {
      gmap = mapFragment.getMap();
      if (gmap != null)
      {
        gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(markerPosition, (float) tweet.zoom));
        gmap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        gmap.setOnMarkerDragListener(this);
        gmap.setOnCameraChangeListener(this);
      }
    }
  }
  
  @Override
  public void onStart()
  {
    super.onStart();
    renderMap(MapHelper.latLng(getActivity(), tweet.geolocation));
    gmap.setOnCameraChangeListener(this);
  }
  
  //----------------------End of Map----------------------------------
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
  {
	    super.onCreateView(inflater, parent, savedInstanceState);
    View v = inflater.inflate(R.layout.fragment_mytweet, parent, false);

    getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
    addListeners(v);
    updateControls(tweet);

    return v;
  } 
  
  private void addListeners(View v)
  {
    tweetText    = (EditText) v.findViewById(R.id.TweetMessage);
    tweetButton  = (Button)   v.findViewById(R.id.TweetButton);
    textCount    = (TextView) v.findViewById(R.id.TweetCharacters);
    dateTime  = (TextView)   v.findViewById(R.id.DateAndTime);

    tweetText.addTextChangedListener(this);
    tweetButton.setOnClickListener(this);
   }
 
  public void updateControls(Tweet tweet)
  {
    tweetText.setText(tweet.message);
    dateTime.setText(tweet.getDateString());
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
    case android.R.id.home: navigateUp(getActivity());
                            return true;
    default:                return super.onOptionsItemSelected(item);
    }
  }
  
  @Override
  public void onPause()
  {
    super.onPause();
    outbox.saveTweets();
    // Get the Intialize location manager
    locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);    // Define the criteria how to select the location provider -> use
    // default
    Criteria criteria = new Criteria();
    provider = locationManager.getBestProvider(criteria, false);
    Location location = locationManager.getLastKnownLocation(provider);
    
    if (location != null) 
    {
        Log.v(TAG, "Provider " + provider + " has been selected.");
        onLocationChanged(location);
      } 
    else 
    {
        Log.v(TAG, "Provider " + provider + " has been not been selected.");
     }
    
    //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, this);
    locationManager.removeUpdates(this);

  }
  
  
  /* Request updates at startup */
  @Override
  public void onResume() 
  {
    super.onResume();
    // Get the Intialize location manager
    locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);    // Define the criteria how to select the location provider -> use
    // default
    Criteria criteria = new Criteria();
    provider = locationManager.getBestProvider(criteria, false);
    Location location = locationManager.getLastKnownLocation(provider);
    
    if (location != null) 
    {
        Log.v(TAG, "Provider " + provider + " has been selected.");
        onLocationChanged(location);
      } 
    else 
    {
        Log.v(TAG, "Provider " + provider + " has been not been selected.");
     }
    
    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 0, this);
    //locationManager.removeUpdates(this);
  }

  
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    if (resultCode != Activity.RESULT_OK)
    {
      return;
    }
    else
      if (requestCode == REQUEST_CONTACT)
      {
        String name = getDisplayName(getActivity(), data);
        String email = getEmail(getActivity(), data);
       // tweet.tweetTarget_Name = name;
        //tweet.tweetTarget_Email = email;
      }   
  }
      
/*
 * Sets the textCounter to a colour depending on value remaining
 */
  @Override
  public void afterTextChanged(Editable s)
  {
    int count = 140 - tweetText.length();
    textCount.setText(Integer.toString(count));
    if (count < 10)
    textCount.setTextColor(Color.RED);
    else
    textCount.setTextColor(Color.GREEN);
    tweet.message = s.toString();
      
    if (count == 0) 
    {  
      Toast.makeText(getActivity(), "Max Characters have been reached!", Toast.LENGTH_SHORT).show();
    }
    
    /*String thisGeolocation = s.toString();
    Log.i(this.getClass().getSimpleName(), "geolocation " + thisGeolocation);
    tweet.geolocation = thisGeolocation;
    getActivity().setTitle(thisGeolocation);
    // using a flag, markerDragged, to avoid race condition
    if (markerDragged == true)
    {
      markerDragged = false;
    }
    else
    {
      renderMap(MapHelper.latLng(getActivity(), thisGeolocation));
    }*/
  }

  
/*
 * Allows the tweetButton to mimic the navigateUp button, and implement tweetButtonPressed()
 */
  @Override
  public void onClick(View v)
  {
    switch (v.getId())
    {
      case R.id.TweetButton:
    	    Log.v(TAG, "USER ID= " + this.app.logged_in_user.uuid.toString() + "Create User");
          MyTweetServiceAPI.createTweet(getActivity(), this, "Sending Tweet", app.logged_in_user, this.tweet);
       /* User user = app.logged_in_user;
        if(user != null)
        {
          MyTweetServiceAPI.createTweet(getActivity(), this, "tweeting", tweet);

        }
        else
        {
          Toast.makeText(getActivity(), "Unable to locate logged-in user", Toast.LENGTH_SHORT).show();
        }*/
          //locationManager.requestLocationUpdates(provider, 400, 1, this);

        break;
    }
  }

  @Override
  public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count)
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void setResponse(List<Tweet> aList)
  {
  }

  @Override
  public void setResponse(Tweet anObject)
  {
    //Toast toast = Toast.makeText(getActivity(), "tweet message successfully sent", Toast.LENGTH_SHORT);
    //toast.show();  
    String msg = "tweet message successfully sent";
    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();  
    LogHelpers.info(this, msg);
  }

  @Override
  public void errorOccurred(Exception e)
  {  
    String msg = "failed to Send tweet";
    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();  
    LogHelpers.info(this, msg + " : " + e.getMessage());
    
  }
  

@Override
public void onLocationChanged(Location location) {
	double longitude = location.getLongitude();
    double latitude = location.getLatitude();
    Log.v(TAG, "longitude " + longitude);
    Log.v(TAG, "latitude " + latitude);
    String thisGeolocation = (String.valueOf(latitude) + "," + String.valueOf(longitude));
    Log.i(this.getClass().getSimpleName(), "geolocation " + thisGeolocation);
    tweet.geolocation = thisGeolocation;
    getActivity().setTitle(thisGeolocation);
    // using a flag, markerDragged, to avoid race condition
    if (markerDragged == true)
    {
      markerDragged = false;
    }
    else
    {
      renderMap(MapHelper.latLng(getActivity(), thisGeolocation));
    }

}

@Override
public void onStatusChanged(String provider, int status, Bundle extras) {
	// TODO Auto-generated method stub
	
}

@Override
public void onProviderEnabled(String provider) {
	Toast.makeText(getActivity(), "Enabled new provider " + provider,
	        Toast.LENGTH_SHORT).show();
	
}

@Override
public void onProviderDisabled(String provider) {
	Toast.makeText(getActivity(), "Disabled provider " + provider,
	        Toast.LENGTH_SHORT).show();	
}

}