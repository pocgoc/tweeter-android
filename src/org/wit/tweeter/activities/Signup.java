package org.wit.tweeter.activities;

import static org.wit.android.helpers.LogHelpers.info;

import java.util.List;
import java.util.logging.Logger;

import org.wit.android.helpers.LogHelpers;
import org.wit.tweeter.R;
import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.controllers.MyTweetServiceAPI;
import org.wit.tweeter.controllers.UsersAPI;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class Signup extends Activity implements Response<User>
{
	private static final String TAG = "SIGNUP";
  private MyTweeterApp app;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_signup);
    app = (MyTweeterApp) getApplication();
  }

  public void registerPressed (View view) 
  {
    TextView firstName = (TextView)  findViewById(R.id.firstName);
    TextView lastName  = (TextView)  findViewById(R.id.lastName);
    TextView email     = (TextView)  findViewById(R.id.Email);
    TextView password  = (TextView)  findViewById(R.id.Password);

    User user = new User (firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(), password.getText().toString());

    UsersAPI.createUser(this, this, "Registering new user", user);
    Log.v(TAG, "Sign up USER= " + user.toString());

  }

  @Override
  public void setResponse(List<User> aList)
  {}

  @Override
  public void setResponse(User user)
  { 
    Log.v(TAG, "Sign up USER= " + user.toString());
    app.users.add(user);
    startActivity (new Intent(this, Welcome.class));
  }

  @Override
  public void errorOccurred(Exception e)
  {
    app.TweeterServiceAvailable = false;
    Toast toast = Toast.makeText(this, "Tweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
    LogHelpers.info(this, toast + " : " + e.getMessage());
    toast.show();
    startActivity (new Intent(this, Welcome.class));
  }



}