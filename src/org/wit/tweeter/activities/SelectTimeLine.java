package org.wit.tweeter.activities;

import java.util.List;

import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.R;
import org.wit.tweeter.http.Response;
import org.wit.tweeter.controllers.MyTweetServiceAPI;
import org.wit.tweeter.controllers.UsersAPI;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class SelectTimeLine extends Activity implements Response<Tweet>
{
  MyTweeterApp app;
  private User user;

  
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_selecttimeline);
    app = (MyTweeterApp) getApplication();
    user   = app.logged_in_user;    
    
    
    //UsersAPI.getUsers(this, this, "Retrieving list of users");
  }
  
  @Override
  public void onResume()
  {
    super.onResume();
    app.logged_in_user = user;
    //UsersAPI.getUsers(this, this, "Retrieving list of users");
  }
  
  void serviceUnavailableMessage()
  {
    Toast toast = Toast.makeText(this, "Your Tweeter Service Unavailable. Try again later", Toast.LENGTH_LONG);
    toast.show();
  }
  
  public void SelectMyTimelinePressed (View view) 
  {
    if (app.TweeterServiceAvailable)
    {
      startActivity (new Intent(this, MyTweetListActivity.class));
    }
    else
    {
      serviceUnavailableMessage();
    } 
  }

  public void SelectPublicTimelinePressed (View view) 
  {
    if (app.TweeterServiceAvailable)
    {
      startActivity (new Intent(this, TweetListActivity.class));
    }
    else
    {
      serviceUnavailableMessage();
    }
  }
  public void SelectUserListPressed (View view) 
  {
    if (app.TweeterServiceAvailable)
    {
      startActivity (new Intent(this, UserListActivity.class));
    }
    else
    {
      serviceUnavailableMessage();
    }
  }
  public void SelectFollowingTimelinePressed (View view) 
  {
	    //MyTweetServiceAPI.getAllTweets(this, this, "Downloading tweets List..");

    if (app.TweeterServiceAvailable)
    {
      startActivity (new Intent(this, FollwedTweetListActivity.class));
    }
    else
    {
      serviceUnavailableMessage();
    }
  }
  public void SelectFollowedUser (View view) 
  {

    if (app.TweeterServiceAvailable)
    {
      startActivity (new Intent(this, FollwedUsersActivity.class));
    }
    else
    {
      serviceUnavailableMessage();
    }
  }
  
  @Override
  public void setResponse(List<Tweet> aList)
  {
    //app.users = aList;
    app.TweeterServiceAvailable = true;
    //app.alltweets     = aList;
    //Toast toast = Toast.makeText(this, "Loaded Tweets!", Toast.LENGTH_LONG);
    //toast.show();

  }
  
  @Override
  public void errorOccurred(Exception e)
  {
    app.TweeterServiceAvailable = false;
    serviceUnavailableMessage();
  }
  

@Override
public void setResponse(Tweet anObject) {
	// TODO Auto-generated method stub
	
}


}

