package org.wit.tweeter.receivers;
import org.wit.android.helpers.LogHelpers;
import org.wit.tweeter.services.RefreshService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class BootReceiver extends BroadcastReceiver
{
  public static int REQUESTCODE = -1;
  private static final long DEFAULT_INTERVAL = AlarmManager.INTERVAL_FIFTEEN_MINUTES;

  @Override
  public void onReceive(Context context, Intent intent)
  {

    android.os.Debug.waitForDebugger(); //this will facilitate stopping at breakpoint placed on code following in this method

    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    long interval = Long.parseLong(prefs.getString("refresh_interval", Long.toString(DEFAULT_INTERVAL)));
    interval *= 60000;//here we convert minutes to milliseconds since input at settings menu is specified in minutes
    interval = interval < 60000 ? 60000 : interval; //Think of the poor server. Denial of service? So we set 60 seconds as the minimum
    PendingIntent operation = PendingIntent.getService(context, REQUESTCODE, new Intent(context, RefreshService.class),
        PendingIntent.FLAG_UPDATE_CURRENT);

    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    alarmManager.cancel(operation);//cancel any existing alarms with matching intent
    alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), interval, operation);
    LogHelpers.info(this, "setting repeat operation for: " + interval);
    LogHelpers.info(this, "onReceived");    
  }

}