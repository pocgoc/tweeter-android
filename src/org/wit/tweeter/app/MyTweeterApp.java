package org.wit.tweeter.app;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.wit.tweeter.models.Follow;
import org.wit.tweeter.models.Portfolio;
import org.wit.tweeter.models.PortfolioSerializer;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import static org.wit.android.helpers.LogHelpers.info;
import android.app.Application;
import android.util.Log;


public class MyTweeterApp extends Application
{
	private static final String FILENAME = "portfolio.json";
	public Portfolio portfolio;
	
	  
	  public List <User>     users        = new ArrayList<User>();  
	  public List <Tweet> tweets    = new ArrayList<Tweet>();
	  public List <Follow> followings    = new ArrayList<Follow>();
	  public List <Tweet> alltweets    = new ArrayList<Tweet>();


	  public boolean         TweeterServiceAvailable = false;
	  public User logged_in_user;
	  
	  
	  
	  public void newUser(User user)
	  {
	    users.add(user);
	  }

	@Override
	public void onCreate()
	{
		super.onCreate();
		PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
		portfolio = new Portfolio(serializer);

		info(this, "TweetControl app launched");
	}
	 
	public User validUser (String email, String password)
	  {
	    for (User user : users)
	    {
	      if (user.email.equals(email) && user.password.equals(password))
	      {
	        logged_in_user = user;
	        return user;
	      }
	    }
	    return null;
	  }
	public User getUser(UUID id)
	{
		Log.i(this.getClass().getSimpleName(), "UUID parameter id: "+ id);

		for (User use : users)
		{
			if(id.equals(use.getId()))
			{
				return use; 
			}
		}
		return null;
	}  
	public Tweet getTweet(UUID id)
	{
		Log.i(this.getClass().getSimpleName(), "UUID parameter id: "+ id);

		for (Tweet twe : tweets)
		{
			if(id.equals(twe.getId()))
			{
				return twe; 
			}
		}
		return null;
	}   
	public Follow getFollow(UUID id)
	{
		Log.i(this.getClass().getSimpleName(), "UUID parameter id: "+ id);

		for (Follow fol : followings)
		{
			if(id.equals(fol.getId()))
			{
				return fol; 
			}
		}
		return null;
	}   
}