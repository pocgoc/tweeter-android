package org.wit.tweeter.httputils;

import java.util.ArrayList;
import java.util.List;

import org.wit.tweeter.models.Follow;
import org.wit.tweeter.models.Tweet;
import org.wit.tweeter.models.User;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class JsonParsers
{
  static Gson gson = new Gson();
  
  public static User json2User(String json)
  {
    return gson.fromJson(json, User.class);   
  }

  public static List<User> json2Users(String json)
  {
    Type collectionType = new TypeToken<List<User>>() {}.getType();
    return gson.fromJson(json, collectionType); 
  }

  public static String user2Json(Object obj)
  {
    return gson.toJson(obj);
  }    

  public static Tweet json2Tweet(String json)
  {
    return gson.fromJson(json, Tweet.class);   
  }

  public static List<Tweet> json2Tweets(String json)
  {
    Type collectionType = new TypeToken<List<Tweet>>() {}.getType();
    return gson.fromJson(json, collectionType); 
  }

  public static String tweet2Json(Object obj)
  {
    return gson.toJson(obj);
  }  
  
  public static Follow json2Follow(String json)
  {
    return gson.fromJson(json, Follow.class);   
  }

  public static List<Follow> json2Followings(String json)
  {
    Type collectionType = new TypeToken<List<Follow>>() {}.getType();
    return gson.fromJson(json, collectionType); 
  }

  public static String follow2Json(Object obj)
  {
    return gson.toJson(obj);
  }    
}

/*public class JsonParsers
{
  public static JSONSerializer userSerializer     = new JSONSerializer().exclude("class")
                                                                        .exclude("persistent")
                                                                        .exclude("entityId"); 
  public static JSONSerializer tweetSerializer = new JSONSerializer().exclude("class")
                                                                        .exclude("persistent")
                                                                        .exclude("entityId"); 
  
  public static User json2User(String json)
  {
    return new JSONDeserializer<User>().deserialize(json, User.class); 
  }

  public static List<User> json2Users(String json)
  {
    return new JSONDeserializer<ArrayList<User>>().use("values", User.class).deserialize(json);
  }
  
  public static String user2Json(Object obj)
  {
    return userSerializer.serialize(obj);
  }
  
  public static List<User> users2Json(String json)
  {
    return new JSONDeserializer<ArrayList<User>>().use("values", User.class).deserialize(json);
  } 
    
 
  public static Tweet json2Tweet(String json)
  {
    return  new JSONDeserializer<Tweet>().deserialize(json, Tweet.class);    
  }

  public static String tweet2Json(Object obj)
  {
    return tweetSerializer.serialize(obj);
  }  
  
  public static List<Tweet> json2Tweets(String json)
  {
    return new JSONDeserializer<ArrayList<Tweet>>().use("values", Tweet.class)
        .deserialize(json);
  }  
}*/