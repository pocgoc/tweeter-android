package org.wit.android.helpers;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;


public class MapHelper
{
  public static LatLng latLng(Context context, String geolocation)
  {
	    Log.v("MAP", "Tweet GeoLocation map helper = " + geolocation);
    String[] g = geolocation.split(",");
    if (g.length == 2)
    {
      return new LatLng(Double.parseDouble(g[0]), Double.parseDouble(g[1]));
    }
    return new LatLng(0, 0);

  }

  public static String latLng(LatLng geo)
  {

    return String.format("%.6f", geo.latitude) + ", " + String.format("%.6f", geo.longitude);
  }

}